package com.devcamp.split;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> SplitString(){
        ArrayList<String> splitArray = new ArrayList<String>();
        String stringValue = "Ok 123 hom nay la thu hai";
        String[] splitString = stringValue.split(" ");
        for(String i : splitString){
            splitArray.add(i);
        }
        return splitArray;
    }
}
